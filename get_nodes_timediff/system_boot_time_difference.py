# coding=utf-8

import os
import argparse
import socket
import time
import json


MAX_RETRIES = 3 # 最大重试次数
RETRY_DELAY = 5 # 重试延迟时间，单位为秒


def update_json(filename, new_json_data):
    """
    更新 JSON 文件
    :param filename: 文件名
    :param new_json_data: 新的 JSON 数据
    """
    # 如果文件不存在，则创建新文件所在的目录
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'r+', encoding='utf-8') as f:
        try:
            old_data = json.load(f)
        except (json.decoder.JSONDecodeError, FileNotFoundError):
            old_data = {}
        # 直接在原始数据上更新
        old_data.update(new_json_data)
        f.seek(0)
        # 写入更新后的数据
        json.dump(old_data, f, indent=4, ensure_ascii=False)


def run_server(port, file_name):
    """
    运行服务器端
    :param port: 端口号
    :param file_name: 输出的 JSON 文件名
    """
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.settimeout(30)
        server_socket.bind(('0.0.0.0', port))
        # 设置监听队列大小为5
        server_socket.listen(5)
        print("等待客户端连接...")

        while True:
            conn, addr = server_socket.accept()
            print(f"Connection from {addr}")
            server_start_time = time.monotonic()
            conn.send(str(server_start_time).encode())
            try:
                client_start_time = conn.recv(1024).decode()
            except socket.timeout:
                print("连接超时，关闭连接")
            else:
                server_end_time = time.monotonic()
                time_difference = float(client_start_time) - \
                    ((server_start_time + server_end_time) / 2)
                print("客户端相对服务器的启动时间差", time_difference, "(s)")

                # 保存客户端IP和启动时间差到JSON文件
                result = {
                    addr[0]: time_difference
                }
                # 追加写入文件
                update_json(file_name, result)
            finally:
                conn.close()

    except socket.timeout:
        print("连接超时，服务器退出")
    except Exception as e:
        print("服务器运行出错:", e)


def run_client(ip, port):
    """
    运行客户端
    :param ip: 服务器 IP 地址
    :param port: 服务器端口号
    """
    retries = 0 # 当前重试次数
    while retries < MAX_RETRIES:
        try:
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.settimeout(10)
            client_socket.connect((ip, port))
            server_start_time = client_socket.recv(1024).decode()
            client_socket.send(str(time.monotonic()).encode())
        except socket.timeout:
            print("连接超时，进行第", retries + 1, "次重试...")
            retries += 1
            # 等待服务器一段时间后进行重试
            time.sleep(RETRY_DELAY)
        except Exception as e:
            print("客户端运行出错:", e)
            return
        else:
            print("连接成功")
            client_socket.close()
            return
    print("客户端重试次数达到最大，连接失败")


def parse_args():
    """
    解析命令行参数
    """
    parser = argparse.ArgumentParser(description='系统启动时间差值测量')
    parser.add_argument('-s', action='store_true', help='启动服务器端')
    parser.add_argument('-c', metavar='SERVER_IP',
                        type=str, help='启动客户端并指定服务器IP')
    parser.add_argument('-p', metavar='PORT', type=int,
                        default=30001, help='服务器端口号,默认为30001')
    parser.add_argument('-o', metavar='OUTPUT_FILE_NAME', type=argparse.FileType('w', encoding='utf-8'),
                        default='time_difference_' + time.strftime('%Y%m%d%H%M%S') + '.json', help='输出的time_difference_*.json文件路径，默认为time_difference_{时间戳}.json')
    args = parser.parse_args()
    if args.o.name == '<stdout>':
        args.o = None
    else:
        args.o = args.o.name
    return args


if __name__ == '__main__':
    args = parse_args()
    if args.s:
        # 启动服务端
        run_server(args.p, args.o)
    elif args.c:
        # 启动客户端
        run_client(args.c, args.p)
    else:
        print("请使用-s启动服务器端或使用-c SERVER_IP启动客户端并指定服务器ip")

