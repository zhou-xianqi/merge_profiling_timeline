#!/bin/bash
set -e

function get_boot_time_difference(){
    # 服务器ip
    SERVER_IP=$1
    CUR_DIR=$(dirname $0)
    # nodeinfo.json文件路径
    NODEINFO_FILE="$CUR_DIR/nodeinfo.json"
    # cluster_common.sh文件路径
    CLUSTER_COMMON_PATH="$CUR_DIR/cluster_common.sh"
    # 服务器端system_boot_time_difference.py文件路径
    SYSTEM_BOOT_TIME_DIFFERENCE_FILE_PATH="$CUR_DIR/system_boot_time_difference.py"
    # sort_time_difference.py文件路径
    SORT_TIME_DIFFERENCE_FILE_PATH="$CUR_DIR/sort_time_difference.py"


    timestamp=$(date "+%Y%m%d%H%M%S")
    # 服务器临时文件夹相对路径
    SERVER_TEMP_DIR_PATH="$CUR_DIR/temp_$timestamp/"
    # 客户端临时文件夹路径
    CLINET_TEMP_DIR_PATH="/tmp/timesync_$timestamp"
    # 服务器time_difference_*.json临时文件路径
    SERVER_TIME_DIFFERENCE_TEMP_PATH="$SERVER_TEMP_DIR_PATH/time_difference_$timestamp.json"
    # 服务器time_difference.json文件路径
    SERVER_TIME_DIFFERENCE_PATH="$CUR_DIR/time_difference.json"


    source $CLUSTER_COMMON_PATH

    # 服务器创建临时文件夹
    mkdir -p $SERVER_TEMP_DIR_PATH
    # 复制脚本到服务器临时目录
    cp $SYSTEM_BOOT_TIME_DIFFERENCE_FILE_PATH $SERVER_TEMP_DIR_PATH

    # 拉起服务器
    python3 "$SERVER_TEMP_DIR_PATH/system_boot_time_difference.py" -s -o $SERVER_TIME_DIFFERENCE_TEMP_PATH &
    server_pid=$!
    # 客户端创建临时文件夹
    cluster_run_cmd_serial "$NODEINFO_FILE" "mkdir -p $CLINET_TEMP_DIR_PATH"
    # 发送文件
    cluster_scp "$NODEINFO_FILE" "$SERVER_TEMP_DIR_PATH"  "$CLINET_TEMP_DIR_PATH"
    # 拉起客户端
    cluster_run_cmd_serial "$NODEINFO_FILE" "python3 $CLINET_TEMP_DIR_PATH/system_boot_time_difference.py -c $SERVER_IP"
    # 终止服务器进程
    kill -9 $server_pid
    # 根据nodeinfo.json调整获取到的时间差
    python3 $SORT_TIME_DIFFERENCE_FILE_PATH -n $NODEINFO_FILE -t $SERVER_TIME_DIFFERENCE_TEMP_PATH -o $SERVER_TIME_DIFFERENCE_PATH
    # 删除服务器临时文件夹
    if [ -d $SERVER_TEMP_DIR_PATH ]; then
        rm -rf $SERVER_TEMP_DIR_PATH
    fi

    # 删除客户端临时文件夹
    cluster_run_cmd_serial "$NODEINFO_FILE" "if [ -d $CLINET_TEMP_DIR_PATH ]; then rm -rf $CLINET_TEMP_DIR_PATH; fi"

}

function main() {
    local server_ip=$1
    get_boot_time_difference "${server_ip}"
}

# 调用main函数
main $1
