# coding=utf-8

import os
import argparse
import json


def sort_time_difference(node_info_file_path, input_time_difference_file_path, output_time_difference_file_path):
    with open(node_info_file_path, 'r+', encoding='utf-8') as f:
        node_info_data = json.load(f)
    with open(input_time_difference_file_path, 'r+', encoding='utf-8') as f:
        input_time_diff_data = json.load(f)
    time_diff_info = {}
    for ip in node_info_data['cluster'].keys():
        time_diff_info[ip] = input_time_diff_data[ip]
    with open(output_time_difference_file_path, 'w+', encoding='utf-8') as f:
        json.dump(time_diff_info, f, indent=4, ensure_ascii=False)


def parse_args():
    parser = argparse.ArgumentParser(description='对系统启动时间差值按照节点排序')
    parser.add_argument('-n', metavar='nodeinfo', help='nodeinfo.json文件路径')
    parser.add_argument('-t', metavar='time_difference_*.json',
                        type=str, help='time_difference_*.json文件路径')
    parser.add_argument('-o', metavar='time_difference.json', type=str,
                        default='time_difference.json', help='输出的time_difference.json文件路径')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    if args.n and args.t:
        sort_time_difference(args.n, args.t, args.o)
    else:
        print("请输入正确的nodeinfo.json与time_difference_*.json文件路径")

