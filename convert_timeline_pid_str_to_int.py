
import sys
import json

def convert(infile, outfile):
    with open(infile, "r+") as f:
        info = json.load(f)
    
    events = info
    for event in events:
        if isinstance(event.get("pid"), str):
            # originpid = event["pid"]
            event["pid"] = int(''.join(x for x in event["pid"] if x.isdigit()))
            # print("ori:{} -> {}".format(originpid, event["pid"]))

        if isinstance(event.get("tid"), str):
            # originpid = event["tid"]
            event["tid"] = int(''.join(x for x in event["tid"] if x.isdigit()))
            # nowpid = event["tid"]
            # print("oritid:{} -> {}".format(originpid, nowpid))

    with open(outfile, 'w') as f:
        json.dump(events, f, ensure_ascii=False)

if __name__ == "__main__":
    infile = sys.argv[1]
    outfile = sys.argv[2]
    convert(infile, outfile)