import sys
import json


def convert(infile, outfile):
    with open(infile, "r+") as f:
        info = json.load(f)

    events = info
    int_id = 1
    replace_id_dict = {}
    for event in events:
        if isinstance(event.get("pid"), str):
            # originpid = event["pid"]
            event["pid"] = int(''.join(x for x in event["pid"] if x.isdigit()))
            # print("ori:{} -> {}".format(originpid, event["pid"]))

        if isinstance(event.get("tid"), str):
            # originpid = event["tid"]
            event["tid"] = int(''.join(x for x in event["tid"] if x.isdigit()))
            # nowpid = event["tid"]
            # print("oritid:{} -> {}".format(originpid, nowpid))

        if event.get('ph', '')=='f' or event.get('ph', '')=='s':
            replaced_id = replace_id_dict.get(event.get('id', ''), 0)
            if replaced_id > 0:
                event['id'] = replaced_id
            else:
                replace_id_dict[event.get('id', '')] = int_id
                event['id'] = int_id
                int_id += 1

    with open(outfile, 'w') as f:
        json.dump(events, f, ensure_ascii=False)


if __name__ == "__main__":
    infile = sys.argv[1]
    outfile = sys.argv[2]
    convert(infile, outfile)
