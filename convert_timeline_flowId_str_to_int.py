
import sys
import json

def convert(infile, outfile):
    with open(infile, "r+") as f:
        info = json.load(f)
    
    events = info
    for event in events:
        if event.get("ph") == "s" or event.get("ph") == "f" or event.get("ph") == "t":
            #originpid = event["id"]
            event["id"] = int(''.join(x for x in event["id"] if x.isdigit()))
            #print("ori:{} -> {}".format(originpid, event["id"]))

    with open(outfile, 'w') as f:
        json.dump(events, f, ensure_ascii=False)

if __name__ == "__main__":
    infile = sys.argv[1]
    outfile = sys.argv[2]
    convert(infile, outfile)